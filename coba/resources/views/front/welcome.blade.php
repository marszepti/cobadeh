<!DOCTYPE html>
<html lang="en">

<head>
    <!-- basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- mobile metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <!-- site metas -->
    <title>Convid</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- bootstrap css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/assets/css/bootstrap.min.css') }}">
    <!-- style css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/assets/css/style.css') }}">
    <!-- Responsive-->
    <link rel="stylesheet" href="{{ asset('frontend/assets/css/responsive.css') }}">
    <!-- fevicon -->
    <link rel="icon" href="{{ asset('frontend/assets/images/fevicon.png') }}" type="image/gif" />
    <!-- Scrollbar Custom CSS -->
    <link rel="stylesheet" href="{{ asset('frontend/assets/css/jquery.mCustomScrollbar.min.css') }}">
    <!-- Tweaks for older IEs-->
    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css')}}">
    <!-- owl stylesheets -->
    <link rel="stylesheet" href="{{ asset('frontend/assets/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/assets/css/owl.theme.default.min.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css')}}"
        media="screen">
</head>

<body>
    <!--header section start -->
    <div class="header_section">
        <div class="container-fluid">
            <div class="main">
                <div class="logo"><a href="index.html"><img src="{{ asset('frontend/assets/images/logo.png') }}"></a>
                </div>
                <div class="menu_text">
                    <ul>
                        <div class="togle_">
                            <div class="menu_main">
                                <ul>
                                    <li><a href="#">Login</a></li>
                                    <li><a href="#"><i class="fa fa-search" aria-hidden="true"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div id="myNav" class="overlay">
                            <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                            <div class="overlay-content">
                                <a href="index.html">Home</a>
                                <a href="protect.html">Protect</a>
                                <a href="about.html">About</a>
                                <a href="doctors.html">Doctors</a>
                                <a href="news.html">News</a>
                            </div>
                        </div>
                        <span class="navbar-toggler-icon"></span>
                        <span onclick="openNav()"><img src="{{ asset('frontend/assets/images/toogle-icon.png') }}"
                                class="toggle_menu"></span>
                        <span onclick="openNav()"><img src="{{ asset('frontend/assets/images/toogle-icon1.png') }}"
                                class="toggle_menu_1"></span>
                    </ul>
                </div>
            </div>
        </div>
        <!-- banner section start -->
        <div class="banner_section layout_padding">
            <div class="container">
                <div id="my_slider" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="container">
                                        <h1 class="banner_taital">Get Medical Care early</h1>
                                        <p class="banner_text">There are many variations of passages of Lorem Ipsum
                                            available, but the majority have suffered alteration in some form.</p>
                                        <div class="more_bt"><a href="#">Read More</a></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="banner_img"><img
                                            src="{{ asset('frontend/assets/images/banner-img.png') }}"></div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="container">
                                        <h1 class="banner_taital">Get Medical Care early</h1>
                                        <p class="banner_text">There are many variations of passages of Lorem Ipsum
                                            available, but the majority have suffered alteration in some form.</p>
                                        <div class="more_bt"><a href="#">Read More</a></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="banner_img"><img
                                            src="{{ asset('frontend/assets/images/banner-img.png') }}"></div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="container">
                                        <h1 class="banner_taital">Get Medical Care early</h1>
                                        <p class="banner_text">There are many variations of passages of Lorem Ipsum
                                            available, but the majority have suffered alteration in some form.</p>
                                        <div class="more_bt"><a href="#">Read More</a></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="banner_img"><img
                                            src="{{ asset('frontend/assets/images/banner-img.png') }}"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#my_slider" role="button" data-slide="prev">
                        <i class="fa fa-angle-left"></i>
                    </a>
                    <a class="carousel-control-next" href="#my_slider" role="button" data-slide="next">
                        <i class="fa fa-angle-right"></i>
                    </a>
                </div>
            </div>
        </div>
        <!-- banner section end -->
    </div>
    <!-- header section end -->
    <!-- protect section start -->
    <div class="protect_section layout_padding">
        <div class="container">
            <div class="row">

            </div>


            <div class="container">

                <div class="row no-gutters">
                    <div class="col-lg-4 col-md-6">
                        <div class="icon-box">
                            <div class="icon"><i class="icofont-computer"></i></div>
                            <h4 class="title"><a href="">Positif</a></h4>
                            <p class="description">{{ $positif }}</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="icon-box">
                            <div class="icon"><i class="icofont-image"></i></div>
                            <h4 class="title"><a href="">Sembuh</a></h4>
                            <p class="description">{{ $sembuh }}</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="icon-box">
                            <div class="icon"><i class="icofont-tasks-alt"></i></div>
                            <h4 class="title"><a href="">Meninggal</a></h4>
                            <p class="description">{{ $meninggal }}</p>
                        </div>
                    </div>
                </div>

            </div>


            <div class="card-body">
                <div class="table-responsive service">
                    <table class="table table-bordered table-hover mb-0 text-nowrap css-serial">
                        <thead>
                            <tr>
                                <th class="atasbro">No.</th>
                                <th class="atasbro">Provinsi</th>
                                <th class="atasbro">Positif</th>
                                <th class="atasbro">Sembuh</th>
                                <th class="atasbro">Meninggal</th>

                            </tr>
                        </thead>
                        <tbody>

                            <tr>
                                <td>&nbsp;</td>
                                <td>DKI Jakarta</td>
                                <td>303,715</td>
                                <td>274,740</td>
                                <td>4,709</td>

                            </tr>

                            <tr>
                                <td>&nbsp;</td>
                                <td>Jawa Barat</td>
                                <td>170,642</td>
                                <td>140,665</td>
                                <td>2,084</td>

                            </tr>

                            <tr>
                                <td>&nbsp;</td>
                                <td>Jawa Tengah</td>
                                <td>138,546</td>
                                <td>89,435</td>
                                <td>5,713</td>

                            </tr>

                            <tr>
                                <td>&nbsp;</td>
                                <td>Jawa Timur</td>
                                <td>119,479</td>
                                <td>105,441</td>
                                <td>8,286</td>

                            </tr>

                            <tr>
                                <td>&nbsp;</td>
                                <td>Sulawesi Selatan</td>
                                <td>50,941</td>
                                <td>46,492</td>
                                <td>775</td>

                            </tr>

                            <tr>
                                <td>&nbsp;</td>
                                <td>Kalimantan Timur</td>
                                <td>46,954</td>
                                <td>37,415</td>
                                <td>1,112</td>

                            </tr>

                            <tr>
                                <td>&nbsp;</td>
                                <td>Riau</td>
                                <td>29,842</td>
                                <td>28,114</td>
                                <td>714</td>

                            </tr>

                            <tr>
                                <td>&nbsp;</td>
                                <td>Bali</td>
                                <td>29,295</td>
                                <td>25,479</td>
                                <td>763</td>

                            </tr>

                            <tr>
                                <td>&nbsp;</td>
                                <td>Sumatera Barat</td>
                                <td>27,802</td>
                                <td>26,239</td>
                                <td>622</td>

                            </tr>

                            <tr>
                                <td>&nbsp;</td>
                                <td>Banten</td>
                                <td>26,770</td>
                                <td>19,582</td>
                                <td>601</td>

                            </tr>

                            <tr>
                                <td>&nbsp;</td>
                                <td>Daerah Istimewa Yogyakarta</td>
                                <td>24,273</td>
                                <td>17,623</td>
                                <td>564</td>

                            </tr>

                            <tr>
                                <td>&nbsp;</td>
                                <td>Sumatera Utara</td>
                                <td>22,286</td>
                                <td>19,212</td>
                                <td>771</td>

                            </tr>

                            <tr>
                                <td>&nbsp;</td>
                                <td>Kalimantan Selatan</td>
                                <td>19,177</td>
                                <td>16,972</td>
                                <td>674</td>

                            </tr>

                            <tr>
                                <td>&nbsp;</td>
                                <td>Papua</td>
                                <td>15,939</td>
                                <td>8,808</td>
                                <td>170</td>

                            </tr>

                            <tr>
                                <td>&nbsp;</td>
                                <td>Sumatera Selatan</td>
                                <td>14,931</td>
                                <td>12,581</td>
                                <td>721</td>

                            </tr>

                            <tr>
                                <td>&nbsp;</td>
                                <td>Sulawesi Utara</td>
                                <td>14,141</td>
                                <td>10,624</td>
                                <td>482</td>

                            </tr>

                            <tr>
                                <td>&nbsp;</td>
                                <td>Kalimantan Tengah</td>
                                <td>12,519</td>
                                <td>11,088</td>
                                <td>331</td>

                            </tr>

                            <tr>
                                <td>&nbsp;</td>
                                <td>Lampung</td>
                                <td>11,151</td>
                                <td>8,569</td>
                                <td>579</td>

                            </tr>

                            <tr>
                                <td>&nbsp;</td>
                                <td>Sulawesi Tenggara</td>
                                <td>9,770</td>
                                <td>8,485</td>
                                <td>186</td>

                            </tr>

                            <tr>
                                <td>&nbsp;</td>
                                <td>Aceh</td>
                                <td>9,373</td>
                                <td>7,754</td>
                                <td>380</td>

                            </tr>

                            <tr>
                                <td>&nbsp;</td>
                                <td>Sulawesi Tengah</td>
                                <td>9,059</td>
                                <td>6,324</td>
                                <td>215</td>

                            </tr>

                            <tr>
                                <td>&nbsp;</td>
                                <td>Kepulauan Riau</td>
                                <td>8,403</td>
                                <td>7,788</td>
                                <td>209</td>

                            </tr>

                            <tr>
                                <td>&nbsp;</td>
                                <td>Kalimantan Utara</td>
                                <td>8,205</td>
                                <td>5,469</td>
                                <td>121</td>

                            </tr>

                            <tr>
                                <td>&nbsp;</td>
                                <td>Nusa Tenggara Barat</td>
                                <td>7,969</td>
                                <td>6,222</td>
                                <td>334</td>

                            </tr>

                            <tr>
                                <td>&nbsp;</td>
                                <td>Papua Barat</td>
                                <td>6,918</td>
                                <td>6,295</td>
                                <td>111</td>

                            </tr>

                            <tr>
                                <td>&nbsp;</td>
                                <td>Maluku</td>
                                <td>6,630</td>
                                <td>5,754</td>
                                <td>101</td>

                            </tr>

                            <tr>
                                <td>&nbsp;</td>
                                <td>Nusa Tenggara Timur</td>
                                <td>6,568</td>
                                <td>3,464</td>
                                <td>176</td>

                            </tr>

                            <tr>
                                <td>&nbsp;</td>
                                <td>Bangka Belitung</td>
                                <td>5,549</td>
                                <td>4,710</td>
                                <td>92</td>

                            </tr>

                            <tr>
                                <td>&nbsp;</td>
                                <td>Jambi</td>
                                <td>4,928</td>
                                <td>3,750</td>
                                <td>76</td>

                            </tr>

                            <tr>
                                <td>&nbsp;</td>
                                <td>Sulawesi Barat</td>
                                <td>4,701</td>
                                <td>2,579</td>
                                <td>87</td>

                            </tr>

                            <tr>
                                <td>&nbsp;</td>
                                <td>Bengkulu</td>
                                <td>4,666</td>
                                <td>4,402</td>
                                <td>145</td>

                            </tr>

                            <tr>
                                <td>&nbsp;</td>
                                <td>Gorontalo</td>
                                <td>4,527</td>
                                <td>4,090</td>
                                <td>122</td>

                            </tr>

                            <tr>
                                <td>&nbsp;</td>
                                <td>Kalimantan Barat</td>
                                <td>4,173</td>
                                <td>3,789</td>
                                <td>32</td>

                            </tr>

                            <tr>
                                <td>&nbsp;</td>
                                <td>Maluku Utara</td>
                                <td>3,713</td>
                                <td>3,018</td>
                                <td>109</td>

                            </tr>



                        </tbody>
                    </table>
                </div>
            </div>

            <div class="protect_section_2 layout_padding">
                <div class="row">
                    <div class="col-md-6">
                        <h1 class="hands_text"><a href="#">Wash your <br>hands frequently</a></h1>
                        <h1 class="hands_text_2"><a href="#">Maintain social <br>distancing</a></h1>
                        <h1 class="hands_text"><a href="#">Avoid touching eyes,<br>nose and mouth</a></h1>
                    </div>
                    <div class="col-md-6">
                        <div class="image_2"><img src="{{ asset('frontend/assets/images/img-2.png') }}"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- protect section end -->
    <!-- about section start -->
    <div class="about_section layout_padding">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="about_img"><img src="{{ asset('frontend/assets/images/img-1.png') }}"></div>
                </div>
                <div class="col-md-6">
                    <h1 class="about_taital">Coronavirus what it is?</span></h1>
                    <p class="about_text">when looking at its layout. The point of using Lorem Ipsum is that it has a
                        more-or-less normal distribution of letters, as opposed to using</p>
                    <div class="read_bt"><a href="#">Read More</a></div>
                </div>
            </div>
        </div>
    </div>
    <!-- about section end -->
    <!-- doctor section start -->
    <div class="doctors_section layout_padding">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="taital_main">
                        <div class="taital_left">
                            <div class="play_icon"><img src="{{ asset('frontend/assets/images/play-icon.png') }}">
                            </div>
                        </div>
                        <div class="taital_right">
                            <h1 class="doctor_taital">What doctors say..</h1>
                            <p class="doctor_text">It is a long established fact that a reader will be distracted by the
                                readable content of a page when looking at its layout. The point of using Lorem Ipsum is
                                that it has a more-or-less normal distribution of letters, as opposed to using 'Content
                                here, content here', making it look</p>
                            <div class="readmore_bt"><a href="#">Read More</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- doctor section end -->
    <!-- news section start -->
    <div class="news_section layout_padding">
        <div class="container">
            <div id="main_slider" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <h1 class="news_taital">Latest News</h1>
                        <p class="news_text">when looking at its layout. The point of using Lorem Ipsum is that it has a
                            more-or-less normal distribution of letters, as opposed to using</p>
                        <div class="news_section_2 layout_padding">
                            <div class="box_main">
                                <div class="image_1"><img src="{{ asset('frontend/assets/images/news-img.png') }}">
                                </div>
                                <h2 class="design_text">Coronavirus is Very dangerous</h2>
                                <p class="lorem_text">It is a long established fact that a reader will be distracted by
                                    the readable content of a page when looking at its layout. The point of using Lorem
                                    Ipsum is that it has a more-or-less normal distribution of letters, as opposed to
                                    using 'Content here, content here', making it look</p>
                                <div class="read_btn"><a href="#">Read More</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <h1 class="news_taital">Latest News</h1>
                        <p class="news_text">when looking at its layout. The point of using Lorem Ipsum is that it has a
                            more-or-less normal distribution of letters, as opposed to using</p>
                        <div class="news_section_2 layout_padding">
                            <div class="box_main">
                                <div class="image_1"><img src="{{ asset('frontend/assets/images/news-img.png') }}">
                                </div>
                                <h2 class="design_text">Coronavirus is Very dangerous</h2>
                                <p class="lorem_text">It is a long established fact that a reader will be distracted by
                                    the readable content of a page when looking at its layout. The point of using Lorem
                                    Ipsum is that it has a more-or-less normal distribution of letters, as opposed to
                                    using 'Content here, content here', making it look</p>
                                <div class="read_btn"><a href="#">Read More</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <h1 class="news_taital">Latest News</h1>
                        <p class="news_text">when looking at its layout. The point of using Lorem Ipsum is that it has a
                            more-or-less normal distribution of letters, as opposed to using</p>
                        <div class="news_section_2 layout_padding">
                            <div class="box_main">
                                <div class="image_1"><img src="{{ asset('frontend/assets/images/news-img.png') }}">
                                </div>
                                <h2 class="design_text">Coronavirus is Very dangerous</h2>
                                <p class="lorem_text">It is a long established fact that a reader will be distracted by
                                    the readable content of a page when looking at its layout. The point of using Lorem
                                    Ipsum is that it has a more-or-less normal distribution of letters, as opposed to
                                    using 'Content here, content here', making it look</p>
                                <div class="read_btn"><a href="#">Read More</a></div>
                            </div>
                        </div>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#main_slider" role="button" data-slide="prev">
                    <i class="fa fa-angle-left"></i>
                </a>
                <a class="carousel-control-next" href="#main_slider" role="button" data-slide="next">
                    <i class="fa fa-angle-right"></i>
                </a>
            </div>
        </div>
    </div>
    </div>
    <!-- news section end -->
    <!-- update section start -->
    <div class="update_section">
        <div class="container">
            <h1 class="update_taital">Get Every Update.... </h1>
            <form action="/action_page.php">
                <div class="form-group">
                    <textarea class="update_mail" placeholder="Massage" rows="5" id="comment" name="Massage"></textarea>
                </div>
                <div class="subscribe_bt"><a href="#">Subscribe Now</a></div>
            </form>
        </div>
    </div>
    <!-- update section end -->
    <!-- footer section start -->
    <div class="footer_section layout_padding">
        <div class="container">
            <div class="footer_section_2">
                <div class="row">
                    <div class="col-lg-3 col-sm-6">
                        <h2 class="useful_text">Resources</h2>
                        <div class="footer_menu">
                            <ul>
                                <li><a href="#">What we do</a></li>
                                <li><a href="#">Media</a></li>
                                <li><a href="#">Travel Advice</a></li>
                                <li><a href="#">Protection</a></li>
                                <li><a href="#">Care</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <h2 class="useful_text">About</h2>
                        <p class="footer_text">Many desktop publishing packages and web page editors now use Lorem Ipsum
                            as their default model text, and a search for 'lorem ipsum' will uncover many web sites
                            still in their infancy. Various</p>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <h2 class="useful_text">Contact Us</h2>
                        <div class="location_text">
                            <ul>
                                <li>
                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>
                                        <span class="padding_15">Location</span></a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-phone" aria-hidden="true"></i>
                                        <span class="padding_15">Call +01 1234567890</span></a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-envelope" aria-hidden="true"></i>
                                        <span class="padding_15">demo@gmail.com</span></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <h2 class="useful_text">countrys</h2>
                        <div class="map_image"><img src="{{ asset('frontend/assets/images/map-bg.png') }}"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- footer section end -->
    <!-- copyright section start -->
    <div class="copyright_section">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <p class="copyright_text">© 2020 All Rights Reserved.<a href="https://html.design"> Free html
                            Templates</a></p>
                </div>
            </div>
        </div>
    </div>
    <!-- copyright section end -->
    <!-- Javascript files-->
    <script src="{{ asset('frontend/assets/js/jquery.min.js') }}"></script>
    <script src="{{ asset('frontend/assets/js/popper.min.js') }}"></script>
    <script src="{{ asset('frontend/assets/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('frontend/assets/js/jquery-3.0.0.min.js') }}"></script>
    <script src="{{ asset('frontend/assets/js/plugin.js') }}"></script>
    <!-- sidebar -->
    <script src="{{ asset('frontend/assets/js/jquery.mCustomScrollbar.concat.min.js') }}"></script>
    <script src="{{ asset('frontend/assets/js/custom.js') }}"></script>
    <!-- javascript -->
    <script src="{{ asset('frontend/assets/js/owl.carousel.js') }}"></script>
    <script src="https:cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js')}}"></script>
    <script>
        $(document).ready(function() {
            $(".fancybox").fancybox({
                openEffect: "none",
                closeEffect: "none"
            });

            $(".zoom").hover(function() {

                $(this).addClass('transition');
            }, function() {

                $(this).removeClass('transition');
            });
        });

    </script>
    <script>
        function openNav() {
            document.getElementById("myNav").style.width = "100%";
        }

        function closeNav() {
            document.getElementById("myNav").style.width = "0%";
        }

    </script>
</body>

</html>
